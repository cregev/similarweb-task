#
# Cookbook Name:: sw-lb
# [Recipe:: nginx]
#---------------
# Installas and configures nginx.conf file for nginx server.
#
# Copyright 2015, SimilarWeb
#
# All rights reserved - Do Not Redistribute
#
# 
include_recipe "nginx"
include_recipe "python"

# [Nginx Config file ]
template "#{node['sw_lb']['nginx_conf_dir']}/nginx.conf" do
  source "nginx.conf.erb"
  mode '0755'
  owner "root"
  group "root"
  action :create
  notifies :reload , "service[nginx]"
  variables(
     :node_group => node['sw_lb']['backend_servers']
  )
end

# ngxtop parses your nginx access log and outputs useful, top-like, metrics of your nginx server. 
#So you can tell what is happening with your server in real-time.
%w{ ngxtop }.each do |pkg|
  python_pip pkg do
    action :install
  end
end


node.run_state['nginx_configure_flags'] =   node.run_state['nginx_configure_flags'] | ['--with-http_stub_status_module']
