#
# Cookbook Name:: sw-lb
# [Recipe:: default]
# Installs SimilarWeb loadbalancer instance
# Copyright 2015, SimilarWeb
#
# All rights reserved - Do Not Redistribute
#
include_recipe "sw-lb::install"
include_recipe "sw-lb::nginx"
include_recipe "sw-lb::gor"
include_recipe "sw-lb::service"





