#
# Cookbook Name:: sw-lb
# [Recipe:: service]
# Instal & Configures gor init.d service
# Copyright 2015, SimilarWeb
#
# All rights reserved - Do Not Redistribute



# [Create Gor init script]
template "/etc/init.d/gor" do
  source "gor.init.erb"
  mode '0755'
  owner "root"
  group "root"
  action :create
  notifies :restart , "service[gor]"
  variables(
     :node_group => node['sw_lb']['backend_servers']
  )
end

service "gor" do
  supports :status => true, :restart => true , :start => true , :stop => true
  action [:enable, :start]
end
