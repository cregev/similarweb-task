#
# Cookbook Name:: sw-lb
# [Recipe:: install]
# Creates Users & Groups ,limits, directories
# Copyright 2015, SimilarWeb
#
# All rights reserved - Do Not Redistribute

# [Create user and group]
group node['sw_lb']['gor_user'] do
  gid node['sw_lb']['gid'] 
  action :create
  system true
end

user node['sw_lb']['gor_user'] do
  comment "gor user"
  home    node['sw_lb']['gor_install_dir']
  shell   "/bin/bash"
  uid     node['sw_lb']['uid'] 
  gid     node['sw_lb']['gor_group']
  supports :manage_home => false
  action  :create
end

# [Root limits]
bash "enable user limits" do
  user 'root'

  code <<-END.gsub(/^    /, '')
    echo 'session    required   pam_limits.so' >> /etc/pam.d/su
  END

  not_if { ::File.read("/etc/pam.d/su").match(/^session    required   pam_limits\.so/) }
end

# [gor user limits]
file "/etc/security/limits.d/10-gor.conf" do
  content <<-END.gsub(/^    /, '')
    #{node['sw_lb'].fetch(:gor_user, "gor")}     -    nofile    #{node['sw_lb']['limits']['nofile'] }
    #{node['sw_lb'].fetch(:gor_user, "gor")}     -    memlock   #{node['sw_lb']['limits']['memlock']}
  END
end
# [Create gor directories]
sw_lb_dirs = []

sw_lb_dirs << node['sw_lb']['gor_logs_dir']
sw_lb_dirs << node['sw_lb']['gor_pid_path'] 

sw_lb_dirs.each do |path|
  directory path do    
    owner node['sw_lb']['gor_user'] and group node['sw_lb']['gor_group'] and mode 0755
    recursive true
    action :create
  end
end