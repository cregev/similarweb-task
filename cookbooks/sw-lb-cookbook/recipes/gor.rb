#
# Cookbook Name:: sw-lb
# [Recipe:: gor]
# Installs & Extracts gor specific defined version and it's configuration
# Copyright 2015, SimilarWeb
#
# All rights reserved - Do Not Redistribute
include_recipe "ark"

# [Install gor with Ark]
 ark "gor" do
   url node['sw_lb']['gor_downlod_url']
   version node['sw_lb']['gor_version']
   append_env_path true
   mode 0755
   strip_components 0 
   action :install
   owner node['sw_lb']['gor_user']
   group node['sw_lb']['gor_group']
   has_binaries ['bin/gor']
 end
