name             'sw-lb'
maintainer       'SimilarWeb'
maintainer_email 'costya.regev@me.com'
license          'All rights reserved'
description      'Installs/Configures SimilarWeb LoadBalancer solution Instance'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'

recipe "sw-lb", "Installs SimilarWeb loadbalancer instance"
recipe "sw-lb::install", "Creates Users & Groups ,limits, directories"
recipe "sw-lb::nginx", "Installs nginx  server and it's configuration file"
recipe "sw-lb::gor", "Installs & Extracts gor specific defined version and it's configuration"
recipe "sw-lb::service","Instal & Configures gor init.d service"



%w{ nginx ark python }.each do |cb|
  depends cb
end

%w{ ubuntu centos debian amazon redhat }.each do |os|
  supports os
end 
