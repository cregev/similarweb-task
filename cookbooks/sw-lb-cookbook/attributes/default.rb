# [Nginx config attributes]
# -------------------------
default['sw_lb']['nginx_conf_dir'] 		= '/etc/nginx'
default['sw_lb']['nginx_server_port'] 	= '80'
default['sw_lb']['nginx_allowed_ip']	= "192.168.1.142"

# [!] Be aware the number of backend servers array will be N<10 ,meaning the maxium servers it can hold we be 9 or less [!]
# [!] The ruby code will only use sthe first 9 servers from the array [!]
default['sw_lb']['backend_servers'] 	= ['192.168.1.42', '192.168.1.43' , '192.168.1.44']

# [LIMITS]
#----------
default['sw_lb']['limits']['memlock']   = 'unlimited'
default['sw_lb']['limits']['nofile']    = '64000'

# [Users & Groups]
#-----------------
default['sw_lb']['gor_user'] 			= "gor"
default['sw_lb']['gor_group'] 		    = "gor"
default['sw_lb']['gid']       		    = nil
default['sw_lb']['uid']         	    = nil
default['sw_lb']['gor_port'] 			= "8081"

# [Gor]
# ------
default['sw_lb']['gor_version'] 		= "0.9.7"
default['sw_lb']['gor_base_url'] 		= "https://github.com/buger/gor/releases/download"
default['sw_lb']['gor_downlod_url']		= "#{node['sw_lb']['gor_base_url']}/#{node['sw_lb']['gor_version']}/#{node['sw_lb']['gor_user']}_x64.tar.gz"

#[Process]
#---------
default['sw_lb']['gor_pid_path'] 		= "/usr/local/run/gor"	
default['sw_lb']['pid_file'] 		    = "#{node['sw_lb']['gor_pid_path']}/gor.pid"

#[Paths]
#-------
default['sw_lb']['gor_install_dir']    	= "/usr/local/gor"
default['sw_lb']['gor_logs_dir'] 		= "/usr/local/var/log/gor"



