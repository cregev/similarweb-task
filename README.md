SimilarWeb LoadBalancer Task



Overview
--------
This project installs & configures a LoadBalancer based on Nginx and Gor.


![Diagram](http://i.imgur.com/55u5K3E.png)


Prerequests:
------------

1. Download & Install [Vagrant][6]
2. Download & Install [Rvm][9], then install the latest ruby version.
3. Download & Install Bundler:  gem install bundler , after installing bundler go to the main path in the project and run: bundle install
4. Download & Install [Chefdk][10]



Setting Up Environemt
---------------------

Launch N<10 Nodes for Checking Post/Get Reqeusts:
----------------------------------------------
1.Go to the Directory servers-php and run the next commands:

* This Command will add the box from the repository to your Vagrant Env.

```bash
 vagrant box add phps package.box 
```

* Check the the box was added to you box list

```bash
vagrant box list
```

* Before running vagrant up , you may choose the amount of the servers you want to run with(Currenlty 3 Servers), just uncomment all the rest server in the node Variable in the Vagrantfile (servers-php/Vagrantfile).Each server will get a public_network ip in order to make it accessable, make sure you add the uncommented nodes ip in  role/sw-lb.json role!!

```bash
vagrant up
```

It will take a couple of minutes for vagrant to create all these nodes please be patient.
Information about the nodes:

All the nodes are preconfigured to start apache server which will recive post/get requests.

Single node installation would have:
------------------------------------

Apache Server:
-------------
1. /var/www/html/1.php basic php script which will get POST requests -> apache server will listen on port 8082
2. /var/www/html/2.php basic php script which will get GET requests  -> apache server will listen on port 8081
3. /var/www/html/upload directory where the scripts above will create a txt file for each post request.

Apache Server Logs:
-------------------

Get logs:
---------
* /var/log/apache2/access.log
* /var/log/apache2/error.log

Post logs:
----------

* /var/log/apache2/access_POST.log
* /var/log/apache2/error_POST.log

Nginx configuration:
--------------------

Nginx is installed on each server to route request of post and get.

/etc/nginx/nginx.conf

1. Nginx listens to port 80
2. Post requests will get to localhost port 8082
3. Get requests will get to localhost port 8081


Main Server Setup(Nginx & Gor):
-------------------------------

Go to the project's main Directoy and run

```bash
vagrant up 
```

This command will launch a sw-lb(Similarweb-LoadBalancer) Vagrant instance with chef-solo which uses the cookbook sw-lb-cookbook.

Chef Cookbook:
--------------
* recipe "sw-lb", "Installs SimilarWeb loadbalancer instance, all the recipes are included in the default recipe"
* recipe "sw-lb::nginx", "Installs nginx  server and it's configuration file"
* recipe "sw-lb::gor", "Downloads,Installs & Extracts gor specific defined version and it's configuration"
* recipe "sw-lb::service","Instal & Configures gor init.d service"
* recipe "sw-lb::install", "Creates Users & Groups ,limits, directories"


Nginx:
------

1. Listens to Port 80
2. Post requests are passed to localhost:8081 , Get request are sent (Round Robin) to a server from a given list of server from the chef template files.

Logs:
-----

Main server:
------------
* access_log /var/log/nginx/access.log;
* error_log /var/log/nginx/error.log;

Dummy Server for Gor:
---------------------
* access_log /var/log/nginx.access.post.log;
* error_log /var/log/nginx.error.post.log;


Gor:
----

1. Listens to Port 8081 for Post requests , when a POST request is recived it's sent & replicated to a list of given servers from the chef template files.


Logs:
-----

* /usr/local/var/log/gor/gor.log
* /usr/local/var/log/gor/gor_error.log


Monitoring:
-----------
* In the above section i have described all the logs paths for each process.
* [ngxtop][7] - ngxtop parses your nginx access log and outputs useful, top-like, metrics of your nginx server. So you can tell what is happening with your server in real-time.
* [Nginx-HttpStubStatusModule][8] - On our main server http://192.168.1.40/nginx_status
* [Gor][4] - Gor can report stats on the output-http request queues.
* Gor also supports exporting response metadata to Elasticsearch , but i did not have enough time to set it up.

Testing:
--------

When running the simplest test on the main node (sw-lb)[192.168.1.40] , i got to a rate of 85 req/sec POST  , and 100 100 req/sec GET

The testing was manual:
I ran on the main node(sw-lb)[192.168.1.40]:

```bash
for i in {1..500}; do  curl 'localhost/' -d "user=try$i" -d 'password=trying' -XPOST ;done 
```
All the resuests were sent to N  nodes.

Then i ran the command:
```bash
for i in {1..500}; do  curl "localhost/?user=try$i";done
```

The Get request were forward to a single server using round robin method.

But's that's is not a real test and of course not a good enougth environment to run tests ... on my local laptop with 4 Vms running.

The way i would do these tests is using Jmeter or Blazemeter which uses Jmeter under the hood and exposes pretty simple UI.




Technologies and libraries used in order to solve this task
------------------------------------------------------------
* [Nginx][2] -  Nginx is a web server with a strong focus on high concurrency, performance and low memory usage. It can also act as a reverse proxy ...
* [Gor][4] - HTTP traffic replay in real-time. Replay traffic from production to staging and dev environments
* [Chef Solo][3] - In order to deploy the code and run the Nginx & Gor server.
* [Vagrant][5] - In order to test the real working Environment on our local laptop.


Contributing
------------

1. Fork the repository on Github
2. Create a named feature branch (like `add_component_x`)
3. Write your change
4. Write tests for your change (if applicable)
5. Run the tests, ensuring they all pass
6. Submit a Pull Request using Github

License and Authors
-------------------
- Author: [CostyaRegev][1] (<Costya.regev@me.com>)


[1]: https://github.com/CostyaRegev
[2]: http://nginx.org/
[3]: https://docs.chef.io/chef_solo.html
[4]: https://github.com/buger/gor
[5]: https://www.vagrantup.com/
[6]: http://www.vagrantup.com/downloads.html
[7]: https://github.com/lebinh/ngxtop
[8]: http://nginx.org/en/docs/http/ngx_http_stub_status_module.html
[9]: https://rvm.io/rvm/install
[10]: https://downloads.chef.io/chef-dk/

